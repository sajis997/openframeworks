#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
   enableDepth = false;

   ofSetVerticalSync(true);

   ofDisableArbTex();

   ofEnableAntiAliasing();
   ofEnableSmoothing();
   
   model.loadModel("teapot.obj",true);
   model.enableNormals();
   model.enableTextures();

   //scale the model
   model.setScale(0.7,0.7,0.7);

   //define the diffuse color that will be
   //interacted by the user
   diffuse_color =  ofFloatColor(0.5,0.2,0.7,1.0);
   
   
   //depth testing makes the teapot
   //rendered properly
   ofEnableDepthTest();

   
   //load the programmable shaders
   if(ofIsGLProgrammableRenderer())
   {
      perFragmentShader.load("shaders/shader-gauraud-fragment");
   }
   else
   {
      std::cout << "Programmable Shading is not supported. " << std::endl;
      exit();
   }

   //define the texture height and width
   texture_width = 512;
   texture_height = 512;


   //declare a fbo setting object
   //to store the fbo setting value
   ofFbo::Settings s;

   //define the fbo settings parameter
   s.width = texture_width;
   s.height = texture_height;
   s.internalformat = GL_RGBA;
   s.useDepth = true;
   s.depthStencilAsTexture = true;

   //allocate buffer for the fbo
   fbo.allocate(s);

   if(!fbo.isAllocated())
   {
      std::cerr << "Fbo allocation was not successful." << std::endl;
      OF_EXIT_APP(0);
   }
   
   fbo.getTextureReference().setTextureWrap( GL_REPEAT, GL_REPEAT );
   
   //the set function will be called internally
   //so we do not need to call the set the function
   box.resizeToTexture(fbo.getTextureReference());

   box.setUseVbo(true);
   
   box.mapTexCoordsFromTexture( fbo.getTextureReference() );


   gui = new ofxUISuperCanvas("Blinn-Phong Parameters");
   
   gui->addSpacer();
   gui->addSlider("Red",0,1,&(diffuse_color.r));
   gui->addSlider("Green",0,1,&(diffuse_color.g));
   gui->addSlider("Blue",0,1,&(diffuse_color.b));

   gui->autoSizeToFitWidgets();
   ofAddListener(gui->newGUIEvent,this,&ofApp::guiEvent);	          
   
}

//--------------------------------------------------------------
void ofApp::update()
{
   float f  = ofGetElapsedTimef() * 60;


   //render starts on the attached texture of the fbo
   fbo.begin();

   //clear the background
   //before the main rendering

   //I WAS HAVING THE TRASPARENCY PROBLEM
   //BECAUSE THE ALPHA VALUE WAS SET TO 50
   //NOW IT IS 255 AND I GOT RID OF ALL THE
   //ARTIFACTS, TACK SJÄLV!!!
   ofBackground(50,50,50,255);

   //re-position the teapot model
   model.setPosition(texture_width/2,
		     texture_height/2,
		     0);

   //animate the model
   model.setRotation(0,f,1,0,0);
   model.setRotation(1,f,0,1,0);
   

   perFragmentShader.begin();
   
   perFragmentShader.setUniform3f("specular_albedo",1.0f,1.0f,1.0f);
   perFragmentShader.setUniform1f("specular_power",30.0f);
   perFragmentShader.setUniform3f("diffuse_albedo",
				  diffuse_color.r,
				  diffuse_color.g,
				  diffuse_color.b);      
   
   //draw the teapot
   model.drawFaces();
   
   perFragmentShader.end();   
   
   fbo.end();
}


void ofApp::exit()
{
   delete gui;
}

void ofApp::guiEvent(ofxUIEventArgs &e)
{

}


//--------------------------------------------------------------
void ofApp::draw()
{
   ofBackground(ofFloatColor(0.5,0.5,0.5,1.0));

   ofFill();
   
   camera.begin();

   //bind the fbo texture we have rendered to
   ((enableDepth) ? fbo.getDepthTexture().bind() : fbo.getTextureReference().bind());

   box.draw();

   ((enableDepth) ? fbo.getDepthTexture().unbind() : fbo.getTextureReference().unbind());

   camera.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
   switch(key)
   {
      case 'd':
	 enableDepth = !enableDepth;
	 break;
      default:
	 break;
   }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
   if(gui->isHit(x,y))
   {
      camera.disableMouseInput();
   }
   else
   {
      camera.enableMouseInput();
   }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
