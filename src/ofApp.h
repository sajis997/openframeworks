#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "ofxUI.h"

class ofApp : public ofBaseApp
{

public:
   void setup();
   void update();
   void draw();
   void exit();
   
   void keyPressed(int key);
   void keyReleased(int key);
   void mouseMoved(int x, int y );
   void mouseDragged(int x, int y, int button);
   void mousePressed(int x, int y, int button);
   void mouseReleased(int x, int y, int button);
   void windowResized(int w, int h);
   void dragEvent(ofDragInfo dragInfo);
   void gotMessage(ofMessage msg);
   
   void guiEvent(ofxUIEventArgs &e);
private:

   ofxUISuperCanvas *gui;
   ofBoxPrimitive box;
   
   ofEasyCam camera;
   
   ofxAssimpModelLoader model;
   ofShader perFragmentShader;
   
   ofFbo fbo;
   
   int texture_width;
   int texture_height;

   bool enableDepth;
   ofFloatColor diffuse_color;
};
