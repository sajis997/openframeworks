#version 150 core

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

in vec4 position;
in vec4 normal;

out VS_OUT
{
   vec3 N;
   vec3 L;
   vec3 V;
} vs_out;

//position of the light
uniform vec3 light_pos = vec3(100.0,100.0,100.0);

void main()
{
	//calculate the view space coordinate
	vec4 P = modelViewMatrix * position;

	//calculate the normal in the view space
	vs_out.N = mat3(modelViewMatrix) * vec3(normal);

	//calculate the light vector
	vs_out.L = light_pos - P.xyz;

	//calculate the view vector
	vs_out.V = -P.xyz;

	//calculate the clip position of each vertex
	gl_Position = projectionMatrix * P;
}